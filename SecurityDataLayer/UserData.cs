﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;

namespace SecurityDataLayer
{
    public class UserData
    {
        public string Login(User user)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
            SqlConnection sqlConnection = new SqlConnection(ConnectionString);
            SqlCommand sqlCommand = new SqlCommand() { Connection = sqlConnection, CommandType = System.Data.CommandType.StoredProcedure };
            sqlCommand.Parameters.AddWithValue("@UserName", user.UserName);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand.CommandText = "Login";
            string dbpass = "";
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())
                {
                    user.ID = sqlDataReader.GetInt32(0);
                    dbpass = sqlDataReader.GetString(1);
                }   
            }
            catch
            {
                return dbpass;
            }
            return dbpass;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using BusinessDataLayer;

namespace BusinessLogicLayer
{
    public class ListLogic
    {
        DataAccess dataAccess = new DataAccess();
        public void Create(SimpleItem item)
        {
            dataAccess.Create(item);
        }

        public List<SimpleItem> Read()
        {
            return dataAccess.Read();
        }

        public void Update(SimpleItem item)
        {
            dataAccess.Update(item);
        }

        public void Delete(SimpleItem item)
        {
            dataAccess.Delete(item);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using SecurityDataLayer;

namespace SecurityLogicLayer
{
    public class UserLogic
    {
        UserData userData = new UserData();
        public bool Login(User user)
        {
            user.Password = new CryptoManager().HashPassword(user.Password);
            string dbPass = userData.Login(user);
            if (dbPass == user.Password)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace SecurityLogicLayer
{
    public class CryptoManager
    {
        public string HashPassword(string password)
        {
            MD5 md5 = MD5.Create();
            
            byte[] inputBytes = Encoding.ASCII.GetBytes(password);
            byte[] hashedPassword = md5.ComputeHash(inputBytes);

            StringBuilder newPassword = new StringBuilder();

            for (int i = 0; i < hashedPassword.Length; i++)
            {
                newPassword.Append(hashedPassword[i].ToString("X2"));
            }

            return newPassword.ToString();
        }
    }
}

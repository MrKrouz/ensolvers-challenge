﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class SimpleItem
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public int ParentID { get; set; }
        public int RelatedUserID { get; set; } = 0;
    }
}

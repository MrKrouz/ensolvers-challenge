using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace Web {

    public class DevExtremeBundleConfig {

        public static void RegisterBundles(BundleCollection bundles) {

            var styleBundle = new StyleBundle("~/Content/DevExtremeBundle");
            var scriptBundle = new ScriptBundle("~/Scripts/DevExtremeBundle");

            // Adding the UTF-8 charset to display icons correctly
            styleBundle.Include("~/Content/Charset.css");

            styleBundle.Include("~/Content/dx.common.css");

            // Predefined themes: https://js.devexpress.com/DevExtreme/Guide/Themes_and_Styles/Predefined_Themes/
            styleBundle.Include("~/Content/dx.material.light-custom.css");

            scriptBundle.Include("~/Scripts/dx.all.js");



            scriptBundle.Include("~/Scripts/aspnet/dx.aspnet.mvc.js");
            scriptBundle.Include("~/Scripts/aspnet/dx.aspnet.data.js");

            bundles.Add(styleBundle);
            bundles.Add(scriptBundle);

#if !DEBUG
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
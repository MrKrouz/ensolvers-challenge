using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Models;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System.Net.Http.Formatting;
using BE;
using BusinessLogicLayer;

namespace Web.Controllers
{
    public class ListController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Get(DataSourceLoadOptions loadOptions)
        {
            List<PruebaModel> lista = new List<PruebaModel>();
            ListLogic listLogic = new ListLogic();

            UserModel userModel = new UserModel();
            LoginController loginController = new LoginController();

            foreach(var item in listLogic.Read())
            {
                lista.Add(new PruebaModel { ID = item.ID, Descripcion = item.Description, parentID = item.ParentID });
            }

            return Request.CreateResponse(DataSourceLoader.Load(lista, loadOptions));
        }

        [HttpPost]
        public HttpResponseMessage Post(FormDataCollection form)
        {
            var values = form.Get("values");

            var newItem = new PruebaModel();
            JsonConvert.PopulateObject(values, newItem);

            ListLogic listLogic = new ListLogic();

            SimpleItem simpleItem = new SimpleItem() {
                Description = newItem.Descripcion,
                ParentID = newItem.parentID
            };

            listLogic.Create(simpleItem);

            return Request.CreateResponse(HttpStatusCode.Created);
        }

        [HttpPut]
        public HttpResponseMessage Put(FormDataCollection form)
        {
            PruebaModel pruebaModel = new PruebaModel();
            var values = form.Get("values");
            pruebaModel.ID = Convert.ToInt32(form.Get("key"));


            JsonConvert.PopulateObject(values, pruebaModel);

            ListLogic listLogic = new ListLogic();

            SimpleItem simpleItem = new SimpleItem()
            {
                ID = pruebaModel.ID,
                Description = pruebaModel.Descripcion
            };

            listLogic.Update(simpleItem);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public void Delete(FormDataCollection form)
        {
            int value = Convert.ToInt32(form.Get("key"));

            ListLogic listLogic = new ListLogic();
            listLogic.Delete(new SimpleItem { ID = value });

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using BE;
using SecurityLogicLayer;

namespace Web.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UserModel model)
        {
            BE.User user = new BE.User();
            user.Password = model.Password;
            user.UserName = model.UserName;

            UserLogic userLogic = new UserLogic();
            
            if (userLogic.Login(user))
            {
                model.Password = string.Empty;
                model.UserID = user.ID;
                Session.Add("userData", model.UserName);
            }
            return RedirectToAction("Index","Home");
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}
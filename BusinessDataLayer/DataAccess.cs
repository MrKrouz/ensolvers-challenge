﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace BusinessDataLayer
{
    public class DataAccess
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        SqlConnection sqlConnection = new SqlConnection();
        SqlCommand sqlCommand = new SqlCommand();
        public void Create(SimpleItem item)
        {
            sqlConnection.ConnectionString = ConnectionString;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandText = "CreateItem";
            sqlCommand.Parameters.AddWithValue("@Description", item.Description);
            sqlCommand.Parameters.AddWithValue("@ParentID", item.ParentID);
            try
            {
                sqlConnection.Open();
                SqlParameter returnv = new SqlParameter("@Id", SqlDbType.Int);
                returnv.Direction = ParameterDirection.Output;
                sqlCommand.Parameters.Add(returnv);
                sqlCommand.ExecuteNonQuery();
                item.ID = (int)returnv.Value;
                sqlConnection.Close();
            }
            catch
            {
                sqlConnection.Close();
            }
        }
        public List<SimpleItem> Read()
        {
            List<SimpleItem> items = new List<SimpleItem>();
            sqlConnection.ConnectionString = ConnectionString;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandText = "ReadItems";
            try
            {
                sqlConnection.Open();
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    items.Add(new SimpleItem { ID = sqlDataReader.GetInt32(0), Description = sqlDataReader.GetString(1), ParentID = sqlDataReader.GetInt32(2) });
                }
                sqlConnection.Close();
                return items;
            }
            catch
            {
                sqlConnection.Close();
                return items;
            }

        }
        public void Update(SimpleItem item)
        {
            sqlConnection.ConnectionString = ConnectionString;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandText = "UpdateItem";
            sqlCommand.Parameters.AddWithValue("@ID", item.ID);
            sqlCommand.Parameters.AddWithValue("@Description", item.Description);
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch
            {
                sqlConnection.Close();
            }
        }
        public void Delete(SimpleItem item)
        {
            sqlConnection.ConnectionString = ConnectionString;
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandText = "DeleteItem";
            sqlCommand.Parameters.AddWithValue("@ID", item.ID);
            try
            {
                sqlConnection.Open();
                sqlCommand.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch
            {
                sqlConnection.Close();
            }
        }
    }
}

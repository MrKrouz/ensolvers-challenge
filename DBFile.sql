USE [Master]
create database EnsolversChallenge
USE [EnsolversChallenge]
GO
/****** Object:  Table [dbo].[Tasks]    Script Date: 15/9/2021 16:24:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasks](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[ParentID] [int] NOT NULL,
	[RelatedUserID] [int] NULL,
	[Active] [bit] NULL CONSTRAINT [DF_Tasks_Active]  DEFAULT ((1)),
 CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 15/9/2021 16:24:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[Logged] [bit] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Tasks] ON 

INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (1, N'Prueba2', 0, NULL, 0)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (2, N'Lunes', 0, NULL, 0)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (3, N'Programar -> Ensolvers Challenge', 2, NULL, 1)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (4, N'Miercoles', 3, NULL, 0)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (5, N'Diseñar -> Ensolvers Challenge', 2, NULL, 1)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (6, N'Martes', 0, NULL, 0)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (7, N'Acomodar estetica -> Ensolvers Challenge', 6, NULL, 1)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (8, N'Lunes', 0, NULL, 0)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (9, N'Lunes', 0, NULL, 1)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (10, N'Diseñar -> Ensolvers Challenge', 9, NULL, 1)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (11, N'Programar -> Ensolvers challenge', 10, NULL, 1)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (12, N'Martes', 0, NULL, 1)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (13, N'Embellecer la web', 12, NULL, 1)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (14, N'Cambiar estilo de colores', 13, NULL, 1)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (15, N'Agregar icono de Ensolvers', 13, NULL, 1)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (16, N'Miercoles', 0, NULL, 1)
INSERT [dbo].[Tasks] ([ID], [Description], [ParentID], [RelatedUserID], [Active]) VALUES (17, N'Llamar a claudia', 16, NULL, 1)
SET IDENTITY_INSERT [dbo].[Tasks] OFF
INSERT [dbo].[Users] ([ID], [UserName], [Password], [Logged]) VALUES (1, N'ensolvers@challenge.com', N'C893BAD68927B457DBED39460E6AFD62', NULL)
/****** Object:  StoredProcedure [dbo].[CreateItem]    Script Date: 15/9/2021 16:24:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[CreateItem](
@Description nvarchar(max),
@ParentID int,
@RelatedUserID int,
@ID int output
)
as
begin
insert into Tasks(Description, ParentID) values (@Description, @ParentID)
set @ID = SCOPE_IDENTITY()
end

GO
/****** Object:  StoredProcedure [dbo].[DeleteItem]    Script Date: 15/9/2021 16:24:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[DeleteItem](
@ID int
)
as
begin
update Tasks set Active = 0 where ID = @ID
end

GO
/****** Object:  StoredProcedure [dbo].[Login]    Script Date: 15/9/2021 16:24:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Login](
@UserName nvarchar(max)
)
as
begin
select ID, Password from Users where UserName = @UserName
end

GO
/****** Object:  StoredProcedure [dbo].[ReadItems]    Script Date: 15/9/2021 16:24:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ReadItems]
as
begin
select ID, Description, ParentID from Tasks where Active = 1
end

GO
/****** Object:  StoredProcedure [dbo].[UpdateItem]    Script Date: 15/9/2021 16:24:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[UpdateItem](
@Description nvarchar(max),
@ID int
)
as
begin
update Tasks set Description = @Description where ID = @ID
end

GO
